# ptdp-astrapay-2022



/**
1. Tambahkan dependency pada pom.xml
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-jdbc</artifactId>
    </dependency>

 Setelah ditambahkan, klik kanan pada pom.xml, maven -> download source dan reload project

 2. Tambahkan credentials data source pada application.properties untuk terhubung dengan database teman-teman

 spring.datasource.url=jdbc:mysql://localhost:3306/nama_db
 spring.datasource.username=root
 spring.datasource.password=

 3. pastikan teman-teman telah memiliki database tersebut dan buat table nya secara manual

 4. Buatlah model tabel kalian

 5. Buat package Repository dan buatlah interface beserta implementasi repository (bisa mengikuti contoh pada referensi https://mkyong.com/spring-boot/spring-boot-jdbc-examples/)

 6. Buatlah API untuk masing-masing save, update, delete, findById, findAll

 */
